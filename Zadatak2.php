<?php

// 2. Napisati PHP funkciju koja pronalazi sve duplikate u datom nizu brojeva.

    function sviDuplikati($originalniNiz)
    {
        $nizDuplikata = [];
        foreach ($originalniNiz as $index1 => $element1) {
            foreach ($originalniNiz as $index2 => $element2) {
                if ($element1 == $element2 && $index1 != $index2) {
                    $nizDuplikata[] = $element1;
                }
            }
        }
        return $nizDuplikata;
    }

?>
