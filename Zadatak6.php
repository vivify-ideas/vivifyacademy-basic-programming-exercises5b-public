<?php

    // 6. Napisati funkciju koja kao parametar prima dvodimenzionalni niz brojeva, i jednodimenzionalni niz stringova koji opisuju kretanje kroz dvodimenzionalni niz, koji mogu da imaju vrednosti 'up', 'down', 'left' i 'right'. Kreće se od pozicije u dvodimenzionalnom nizu [0][0], koji predstavlja element gore levo. Proći kroz niz sa stringovima i kretati se kroz dvodimenzionalni niz u skladu sa trenutnim stringom (ako naiđemo na 'down' krećemo se nadole itd). Nakon što prođe kroz niz stringova, funkcija treba da vrati element niza na kom se nalazi. Primer:
    //
    //     Ako se prosledi dvodimenzionalni niz:
    //         [
    //             [1, 2, 3, 4],
    //             [5, 6, 7, 8],
    //             [9, 10, 11, 12],
    //             [13, 14, 15, 16]
    //         ]
    //
    // i niz stringova
    //
    // ['down', 'down', 'right', 'down']
    //
    // funkcija treba da vrati 14 (kreće se od 1, ide korak nadole na 5, još jedan korak nadole na 9, zatim desno na 10, i ponovo dole na 14.
    //
    // Nije potrebno voditi računa o “izlaženju” van granica niza, pretpostaviti da će niz stringova sa koracima uvek biti formiran tako da se ostane unutar granica.

    function kretanjeKrozNiz($niz2D, $koraci)
    {
        $x = 0;
        $y = 0;
        foreach ($koraci as $korak) {
            if ($korak == 'down') {
                $y++;
            } else if ($korak == 'up') {
                $y--;
            } else if ($korak == 'right') {
                $x++;
            } else if ($korak == 'left') {
                $x--;
            }
        }
        return $niz2D[$y][$x];
    }

?>
