<?php

    // 3. Napisati funkciju koja kao parametar prima 4x4 dvodimenzionalni niz sa brojevima, sledeće strukture:
    // [
    //     [10, 14, 6, 1],
    //     [4, 12, 2, 50],
    //     [21, 8, 17, 3],
    //     [6, 9, 15, 11]
    // ]
    //
    // Vrednost koju funkcija vraća je zbir svih brojeva u nizovima (u ovom slučaju 189).

    function zbirDvodimenzionalnog4x4($niz)
    {
        $zbir = 0;
        for ($i = 0; $i < 4; $i++) {
            for ($j = 0; $j < 4; $j++) {
                $zbir += $niz[$i][$j];
            }
        }
        return $zbir;
    }

?>
